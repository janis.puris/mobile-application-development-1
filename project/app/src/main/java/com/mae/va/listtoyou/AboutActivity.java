package com.mae.va.listtoyou;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import static com.mae.va.listtoyou.R.id.action_main;
import static com.mae.va.listtoyou.R.id.action_exit;

/**
 * Created by Arturs on 01.06.2017.
 */

public class AboutActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        this.setTitle(getResources().getText(R.string.action_settings));

        listView = (ListView) findViewById(R.id.list_members);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(AboutActivity.this, MemberActivity.class);
                intent.putExtra((String) getResources().getText(R.string.member_name), listView.getItemAtPosition(position).toString());
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.about_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == action_main) {
            startActivity(new Intent(AboutActivity.this, MainActivity.class));
            return true;
        }
        if (id == action_exit) {
            ExitApp();
            return true;
        }
        if (id == R.id.map) {
            startActivity(new Intent(AboutActivity.this, MapsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void ExitApp(){
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.exit_application_check));
        builder.setCancelable(true);
        builder.setPositiveButton(getResources().getString(R.string.button_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                finishAffinity();
            }
        }).setNegativeButton(getResources().getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

}
