package com.mae.va.listtoyou;

import android.app.Dialog;
import android.app.LauncherActivity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static com.mae.va.listtoyou.R.id.action_exit;
import static com.mae.va.listtoyou.R.id.action_main;
import static com.mae.va.listtoyou.R.id.action_settings;
import static com.mae.va.listtoyou.R.layout.activity_main;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    // Add task button
    FloatingActionButton addTask;

    // Task list in main activity
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    // Task list array to hold values for insert into Task list widget (recyclerviewer)
    ArrayList<Task> taskList = new ArrayList<>();

    // DB object that will do CRUD on SQLite
    SQLiteModel sqlite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new TaskListAdapter(taskList, getApplicationContext());
        recyclerView.setAdapter(adapter);

        addTask = (FloatingActionButton) findViewById(R.id.fab_addTask);
        addTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.activity_task);
                dialog.show();

                // Dialog title
                TextView tvDialogTitle = (TextView) dialog.findViewById(R.id.tv_dialog_title);
                tvDialogTitle.setText("Add Task");

                // Task completion state
                Switch swDone = (Switch) dialog.findViewById(R.id.sw_done);
                swDone.setVisibility(View.GONE);

                // Dialog DISCARD button
                Button cancel = (Button) dialog.findViewById(R.id.btn_discard);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                // Dialog SAVE button
                Button save = (Button) dialog.findViewById(R.id.btn_save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Getting the Title and Note text values from dialog
                        EditText taskTitle = (EditText) dialog.findViewById(R.id.et_title);
                        EditText taskNote = (EditText) dialog.findViewById(R.id.et_note);

                        // Lets check if the Title is not blank (note is optional)
                        if (taskTitle.getText().length() > 0) {

                            // Messy, but checks, if any radio button is checked, and if it is, it will determine what is the selected radio buttons text property
                            RadioGroup priority = (RadioGroup) dialog.findViewById(R.id.rbg_priority);
                            String priorityText = new String();
                            if (priority.getCheckedRadioButtonId() != -1) {
                                View radioButtonGroupView = priority.findViewById(priority.getCheckedRadioButtonId());
                                int radioButtonSelectedId = priority.indexOfChild(radioButtonGroupView);
                                RadioButton radioButtonId = (RadioButton) priority.getChildAt(radioButtonSelectedId);
                                priorityText = (String) radioButtonId.getText();
                            }

                            // Compose the ContentValues object to store the data. This object will be used to deliver the data to SQLiteModel
                            // More on https://developer.android.com/reference/android/content/ContentValues.html

                            ContentValues contentValues = new ContentValues();
                            contentValues.put("Title", taskTitle.getText().toString());
                            contentValues.put("Priority", priorityText);
                            contentValues.put("Status", "Incomplete");
                            contentValues.put("Note", taskNote.getText().toString());

                            // DEBUG Toast the prioritytext :)
                            // Toast.makeText(getApplicationContext(),priorityText , Toast.LENGTH_LONG).show();

                            // Save the input data to db
                            sqlite = new SQLiteModel(getApplicationContext());
                            Boolean insertQuerySuccess = sqlite.insertInto(contentValues);

                            if (insertQuerySuccess) {
                                dialog.hide();
                                updateCardView();
                            } else {
                                Toast.makeText(getApplicationContext(), "Error: Could not save changes!", Toast.LENGTH_SHORT).show();
                            }

                            // Since we are done with dialog, we will hide it from user.
                            dialog.hide();
                            Toast.makeText(getApplicationContext(), "Task is added !!", Toast.LENGTH_SHORT).show();

                        } else {
                            // The mandatory fields are not filled.
                            Toast.makeText(getApplicationContext(), "Please enter the task title!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        updateCardView();
    }

    public void ExitApp(){
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.exit_application_check));
        builder.setCancelable(true);
        builder.setPositiveButton(getResources().getString(R.string.button_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                finishAffinity();
            }
        }).setNegativeButton(getResources().getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void updateCardView() {
        sqlite = new SQLiteModel(getApplicationContext());
        Cursor result = sqlite.selectAllData();

        if (result.getCount() == 0) {
            taskList.clear();
            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(), "No Tasks", Toast.LENGTH_SHORT).show();
        } else {
            taskList.clear();
            adapter.notifyDataSetChanged();
            while (result.moveToNext()) {

                Task task = new Task();
                task.setTaskID(result.getInt(0));
                task.setTaskTitle(result.getString(1));
                task.setTaskPriority(result.getString(2));
                task.setTaskStatus(result.getString(3));
                task.setTaskNote(result.getString(4));

                // DEBUG
                Log.d(TAG, "SQLite cursor result: " + taskList.toString());

                taskList.add(task);
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == action_settings) {
            startActivity(new Intent(MainActivity.this, AboutActivity.class));
            return true;
        }

        if (id == R.id.map) {
            startActivity(new Intent(MainActivity.this, MapsActivity.class));
            return true;
        }

        if (id == R.id.btn_choose_image) {
            startActivity(new Intent(MainActivity.this, CameraActivity.class));
            return true;
        }

        if (id == R.id.btnAudio) {
            startActivity(new Intent(MainActivity.this, AudioActivity.class));
            return true;
        }

        if (id == R.id.btnWeb) {
            startActivity(new Intent(MainActivity.this, WebActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed(){
        ExitApp();
    }

}
