package com.mae.va.listtoyou;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import static com.mae.va.listtoyou.R.layout.activity_main;
import static com.mae.va.listtoyou.R.layout.card_task;

/**
 * Created by Arturs on 30.05.2017.
 */


public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.TaskListViewHolder> {

    // Constants
    private static final String TAG = "TaskListAdapter";

    // DB object that will do CRUD on SQLite
    SQLiteModel sqlite;

    List<Task> TaskListItems = new ArrayList<>();
    Context context;

    public TaskListAdapter(List<Task> taskListItems, Context context) {
        this.TaskListItems = taskListItems;
        this.context = context;
    }


    @Override
    public TaskListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_task, parent, false);
        return new TaskListViewHolder(v, context);
    }

    @Override
    public void onBindViewHolder(final TaskListViewHolder holder, final int position) {
        final Task task = TaskListItems.get(position);

        final String taskStatus = task.getTaskStatus();
        final String taskPriority = task.getTaskPriority();
        final int taskId = task.getTaskID();

        int color = 0;

        holder.txtViewTaskTitle.setText(task.getTaskTitle());
        holder.txtViewTaskNote.setText(task.getTaskNote());

        // If Task is done, then change title + make green world :D
        if (taskStatus.matches("Complete")) {
            color = Color.parseColor("#9ee300");
            holder.txtViewTaskTitle.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            holder.priorityView.setBackgroundColor(color);
            holder.priorityView.getLayoutParams().width =  RecyclerView.LayoutParams.MATCH_PARENT;

        } else {
            holder.txtViewTaskTitle.setPaintFlags(0);
            holder.priorityView.getLayoutParams().width = 15;

            // Depending on task prio, we are going to assign different accent color
            switch (taskPriority) {
                case "Low":
                    color = Color.parseColor("#33AA77");
                    break;
                case "Normal":
                    color = Color.parseColor("#009EE3");
                    break;
                case "High":
                    color = Color.parseColor("#FF7799");
                    break;
            }
            holder.priorityView.setBackgroundColor(color);

        }

        // Clicking on task card (Edit task)
        holder.cvTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Task card clicked with id: " + taskId);

                // todo The below is copy from mainActivity, this is stupid, @arturs, we should get this into its own method or something!

                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.activity_task);
                dialog.show();

                // Dialog title
                TextView tvDialogTitle = (TextView) dialog.findViewById(R.id.tv_dialog_title);
                tvDialogTitle.setText("Edit Task");

                // Task EditText boxes
                EditText etTaskTitle = (EditText) dialog.findViewById(R.id.et_title);
                EditText etTaskNote = (EditText) dialog.findViewById(R.id.et_note);

                // Task priority radio buttons
                RadioButton rbHigh = (RadioButton) dialog.findViewById(R.id.rb_priority_high);
                RadioButton rbNormal = (RadioButton) dialog.findViewById(R.id.rb_priority_normal);
                RadioButton rbLow = (RadioButton) dialog.findViewById(R.id.rb_priority_low);

                // Task completion state
                final Switch swDone = (Switch) dialog.findViewById(R.id.sw_done);

                // Dialog DISCARD button
                Button cancel = (Button) dialog.findViewById(R.id.btn_discard);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                // Lets set the task priority for dialog
                switch (taskPriority) {
                    case "Low": rbLow.setChecked(true);
                        break;
                    case "Normal": rbNormal.setChecked(true);
                        break;
                    case "High": rbHigh.setChecked(true);
                        break;
                }

                // Lets set the task status for dialog
                if (task.getTaskStatus().matches("Complete")) {
                    swDone.setChecked(true);
                }

                // Lets set the Title and Note
                etTaskTitle.setText(task.getTaskTitle());
                etTaskNote.setText(task.getTaskNote());

                // Dialog SAVE button
                Button save = (Button) dialog.findViewById(R.id.btn_save);
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Getting the Title and Note text values from dialog
                        EditText taskTitle = (EditText) dialog.findViewById(R.id.et_title);
                        EditText taskNote = (EditText) dialog.findViewById(R.id.et_note);

                        // Lets check if the Title is not blank (note is optional)
                        if (taskTitle.getText().length() > 0) {

                            // Messy, but checks, if any radio button is checked, and if it is, it will determine what is the selected radio buttons text property
                            RadioGroup priority = (RadioGroup) dialog.findViewById(R.id.rbg_priority);
                            String priorityText = new String();
                            if (priority.getCheckedRadioButtonId() != -1) {
                                View radioButtonGroupView = priority.findViewById(priority.getCheckedRadioButtonId());
                                int radioButtonSelectedId = priority.indexOfChild(radioButtonGroupView);
                                RadioButton radioButtonId = (RadioButton) priority.getChildAt(radioButtonSelectedId);
                                priorityText = (String) radioButtonId.getText();
                            }

                            // We will need new task object that will hold the new values. We can not reuse the task object because its final. :))
                            Task updatedTask = new Task();

                            // Lets fill up the updated task object
                            updatedTask.setTaskID(task.getTaskID());
                            updatedTask.setTaskTitle(taskTitle.getText().toString());
                            updatedTask.setTaskPriority(priorityText);

                            if(swDone.isChecked()) {
                                updatedTask.setTaskStatus("Complete");
                            } else {
                                updatedTask.setTaskStatus("Incomplete");
                            }

                            updatedTask.setTaskNote(taskNote.getText().toString());

                            // Give the new task object to the SQLiteModel.updateTask method
                            sqlite = new SQLiteModel(v.getContext());
                            Cursor cursor = sqlite.updateTask(updatedTask);

                            // Refresh recyclerview with notifyDataSetChanged
                            TaskListItems.set(position, updatedTask);
                            if (cursor.getCount() == 0) {

                                new Handler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        notifyDataSetChanged();
                                    }
                                });

                                dialog.hide();
                                 Toast.makeText(v.getContext(), "Task is refreshed !", Toast.LENGTH_LONG).show();
                            } else {
                                dialog.hide();
                            }

                        } else {
                            // The mandatory fields are not filled.
                            Toast.makeText(v.getContext(), "Please enter the task title!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });


        holder.cvTask.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(v.getContext());
                mBuilder.setTitle((R.string.dialog_title) );

                // Dialog OK button
                mBuilder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // For DEBUG
                        //Toast.makeText(context, "OnLongClick Called at position " + position, Toast.LENGTH_SHORT).show();

                        int id = task.getTaskID();
                        SQLiteModel sqlite = new SQLiteModel(context);
                        Cursor cursor = sqlite.deleteTask(id);
                        if (cursor.getCount() == 0) {
                            Toast.makeText(context,holder.txtViewTaskTitle.getText().toString() +  " Deleted", Toast.LENGTH_SHORT).show();
                            new Handler().post(new Runnable() {
                                @Override
                                public void run() {
                                    TaskListItems.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position,TaskListItems.size());
                                    notifyDataSetChanged();
                                }
                            });
                        } else {
                            Toast.makeText(context, holder.txtViewTaskTitle.getText().toString() + " removed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                // Dialog Cancel button
                mBuilder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog alert = mBuilder.create();
                alert.show();
                return true;
            }


        });
    }

    @Override
    public int getItemCount() {
        return TaskListItems.size();
    }

    public class TaskListViewHolder extends RecyclerView.ViewHolder{

        public TextView txtViewTaskTitle;
        public TextView txtViewTaskNote;
        public CardView cvTask;
        public ImageView priorityView;

        public TaskListViewHolder(View v, final Context context) {
            super(v);

            txtViewTaskTitle = (TextView) v.findViewById(R.id.txtViewTaskTitle);
            txtViewTaskNote = (TextView) v.findViewById(R.id.txtViewTaskNote);
            cvTask = (CardView) v.findViewById(R.id.cvTask);
            priorityView = (ImageView) v.findViewById(R.id.priorityView);
        }
    }


}
