package com.mae.va.listtoyou;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import static com.mae.va.listtoyou.R.id.action_main;
import static com.mae.va.listtoyou.R.id.action_settings;

public class WebActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        WebView webView = new WebView(this);
        setContentView(webView);
        webView.loadUrl("https://www.1188.lv/laika-zinas/valmiera");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.audio_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == action_main) {
            startActivity(new Intent(WebActivity.this, MainActivity.class));
            return true;
        }

        if (id == action_settings) {
            startActivity(new Intent(WebActivity.this, AboutActivity.class));
            return true;
        }

        if (id == R.id.btn_choose_image) {
            startActivity(new Intent(WebActivity.this, CameraActivity.class));
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

}
