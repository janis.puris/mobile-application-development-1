package com.mae.va.listtoyou;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.util.Log;

/**
 * Created by janis on 2017-05-30.
 */

public class SQLiteModel extends SQLiteOpenHelper {

    // Constants
    private static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "ListToYou.db";
    public static final String DATABASE_TABLE = "Tasks";
    public static final String KEY_ID = "ID";
    public static final String KEY_TITLE = "Title";
    public static final String KEY_PRIORITY = "Priority";
    public static final String KEY_STATUS = "Status";
    public static final String KEY_NOTE = "Note";
    private static final String TAG = "SQLite";


    // Create DB to store To Do Tasks
    public SQLiteModel(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    // Create Table
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableQuery = "create table if not exists " + DATABASE_TABLE + " ( ID INTEGER PRIMARY KEY AUTOINCREMENT, Title TEXT, Priority TEXT, Status TEXT, Note TEXT)";
        db.execSQL(createTableQuery);
        Log.d(TAG,"OnCreate done");
    }

    // Recreate Table, on upgrade
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        onCreate(db);
        Log.d(TAG,"onUpgrade done");
    }

    // Insert into Table
    public boolean insertInto(ContentValues cv) {
        SQLiteDatabase db = this.getWritableDatabase();
        long results = db.insert(DATABASE_TABLE, null, cv);
        if (results == -1) {
            return false;
        } else {
            return true;
        }
    }

    // Get all entries
    public Cursor selectAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        //String query = "select * from " + DATABASE_TABLE;
        String query = "select ID, Title, Priority, Status, Note, case (Priority) when \"High\" then 0 when \"Normal\" then 1 when \"Low\" then 3 else 4 end as PriorityId from " + DATABASE_TABLE + " order by PriorityId, id";
        Cursor result = db.rawQuery(query, null);
        Log.d(TAG,"selectAllData done");
        return result;
    }

    // Update specific Task
    public Cursor updateTask(Task tdo) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE "
                + DATABASE_TABLE
                + " SET "
                + KEY_TITLE + "='" + tdo.getTaskTitle()
                + "', "
                + KEY_PRIORITY + "='" + tdo.getTaskPriority()
                + "', "
                + KEY_STATUS + "='" + tdo.getTaskStatus()
                + "', "
                + KEY_NOTE + "='" + tdo.getTaskNote()
                + "' WHERE " + KEY_ID + "='" + tdo.getTaskID() + "'";
        Cursor results = db.rawQuery(query, null);
        Log.d(TAG,"updateTask done");
        return results;
    }

    // Delete specific Task
    public Cursor deleteTask(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + DATABASE_TABLE
                + " WHERE "
                + KEY_ID + "='"
                + id + "'";
        Cursor result = db.rawQuery(query, null);
        Log.d(TAG,"deleteTask done");
        return result;
    }

}
