package com.mae.va.listtoyou;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import static com.mae.va.listtoyou.R.id.action_exit;
import static com.mae.va.listtoyou.R.id.action_main;
import static com.mae.va.listtoyou.R.id.action_settings;


/**
 * Created by Arturs on 01.06.2017.
 */

public class MemberActivity extends AppCompatActivity {

    ImageView Portret;
    TextView Email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_member);

        Portret = (ImageView) findViewById(R.id.img_profile);
        Email  = (TextView) findViewById(R.id.txt_email);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.setTitle(bundle.getString((String) getResources().getText(R.string.member_name)));
            if (this.getTitle().toString().equalsIgnoreCase((String) getResources().getText(R.string.member_arturs))) {
                Portret.setImageDrawable(ContextCompat.getDrawable(MemberActivity.this, R.drawable.ar));
                Email.setText(getResources().getText(R.string.member_arturs_email));
            } else if (this.getTitle().toString().equalsIgnoreCase((String) getResources().getText(R.string.member_janis))) {
                Portret.setImageDrawable(ContextCompat.getDrawable(MemberActivity.this, R.drawable.jp));
                Email.setText(getResources().getText(R.string.member_janis_email));
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.member_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == action_settings) {
            startActivity(new Intent(MemberActivity.this, AboutActivity.class));
            return true;
        }
        if (id == action_main) {
            startActivity(new Intent(MemberActivity.this, MainActivity.class));
            return true;
        }
        if (id == action_exit) {
            ExitApp();
            return true;
        }
        if (id == R.id.map) {
            startActivity(new Intent(MemberActivity.this, MapsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void ExitApp(){
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.exit_application_check));
        builder.setCancelable(true);
        builder.setPositiveButton(getResources().getString(R.string.button_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                finishAffinity();
            }
        }).setNegativeButton(getResources().getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

}

