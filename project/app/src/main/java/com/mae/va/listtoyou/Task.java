package com.mae.va.listtoyou;

/**
 * Created by janis on 2017-05-30.
 */

// Very simple object to hold single task details. I believe no more commentary is necessary :)

public class Task {
    private int taskId;
    private String taskTitle, taskPriority, taskStatus, taskNote;

    public int getTaskID() {
        return taskId;
    }

    public String getTaskNote() {
        return taskNote;
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public String getTaskPriority() {
        return taskPriority;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskID(int taskId) {
        this.taskId = taskId;
    }

    public void setTaskNote(String taskNote) {
        this.taskNote = taskNote;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public void setTaskPriority(String taskPriority) {
        this.taskPriority = taskPriority;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    // For DEBUG purposes
    @Override
    public String toString() {
        return "Data {id: " + taskId + ", taskTitle: " + taskTitle + ", priority: " + taskPriority + ", status: " + taskStatus + ", notes: " + taskNote + "}";
    }
}
