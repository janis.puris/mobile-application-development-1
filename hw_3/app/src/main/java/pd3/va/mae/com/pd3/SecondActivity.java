package pd3.va.mae.com.pd3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {
    public static final String DEFAULT="";

    TextView dataView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        this.setTitle(getResources().getString(R.string.title_activity_second));

        dataView = (TextView) findViewById(R.id.dataTextView);
    }

    public void First_Activity(View view)
    {
        Intent intent = new Intent(SecondActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public void getData(View view){
        SharedPreferences savedData = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        String savedInput = savedData.getString("userDataInput",DEFAULT);


        if(savedInput.equals(DEFAULT)) {
            Toast toast= Toast.makeText(this,getResources().getString(R.string.toast_not_set),Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }else{
            dataView.setText(savedInput) ;
        }
        }


}
