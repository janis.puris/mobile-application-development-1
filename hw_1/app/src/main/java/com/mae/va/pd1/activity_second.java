package com.mae.va.pd1;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class activity_second extends AppCompatActivity {

    Toolbar mToolbar;
    ImageView Portret;
    TextView Email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_members);
        Portret = (ImageView) findViewById(R.id.img_profile);
        Email  = (TextView) findViewById(R.id.txt_email);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mToolbar.setTitle(bundle.getString((String) getResources().getText(R.string.member_name)));
            if (mToolbar.getTitle().toString().equalsIgnoreCase((String) getResources().getText(R.string.member_arturs))) {
                Portret.setImageDrawable(ContextCompat.getDrawable(activity_second.this, R.drawable.ar));
                Email.setText(getResources().getText(R.string.member_arturs_email));
            } else if (mToolbar.getTitle().toString().equalsIgnoreCase((String) getResources().getText(R.string.member_janis))) {
                Portret.setImageDrawable(ContextCompat.getDrawable(activity_second.this, R.drawable.jp));
                Email.setText(getResources().getText(R.string.member_janis_email));
            }
        }
    }
}
