package pd2.va.mae.com.pd2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Second_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        this.setTitle(getResources().getString(R.string.title_activity_second));
    }

    public void First_Activity(View view)
    {
        Intent intent = new Intent(Second_Activity.this, activity_main.class);
        startActivity(intent);
    }

}
