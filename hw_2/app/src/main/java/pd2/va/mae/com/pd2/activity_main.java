package pd2.va.mae.com.pd2;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

@SuppressWarnings("deprecation")
public class activity_main extends AppCompatActivity {

    Button btnShowDialog;

    TextView dialogItemSelected;
    String[] listItems;
    boolean[] checkedItems;
    ArrayList<Integer> userSelectedItems = new ArrayList();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setTitle(getResources().getString(R.string.title_activity_main));

        // Init Button and TextView objects
        btnShowDialog = (Button) findViewById(R.id.btnShowDialog);

        // Fill array with string values
        listItems = getResources().getStringArray(R.array.dialog_txt_array);

        // Init checked item list (boolean)
        checkedItems = new boolean[listItems.length];

        // Button dialog show click event
        btnShowDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity_main.this);
                mBuilder.setTitle(getResources().getString(R.string.dialog_title));

                // Dialog multiChoice with on click event for each checkbox
                mBuilder.setMultiChoiceItems(listItems, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            userSelectedItems.add(which);
                        }else if(userSelectedItems.contains(which) ){
                            userSelectedItems.remove(Integer.valueOf(which));
                        }
                    }
                });
                // Dialog OK button
                mBuilder.setPositiveButton(getResources().getText(R.string.button_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       onOkay(userSelectedItems);
                    }
                });
                // Dialog Cancel button
                mBuilder.setNegativeButton(getResources().getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = mBuilder.create();
                alert.show();
            }
        });
    }

    protected void onOkay(ArrayList<Integer> arrayList){
        StringBuilder stringBuilder  =new StringBuilder();
        if(arrayList.size() != 0){
            for( int i = 0; i < arrayList.size(); i++ ){
                String members = listItems[arrayList.get(i)];
                stringBuilder = stringBuilder.append(System.getProperty("line.separator") + members + " " +  getResources().getString(R.string.name_activity));
            }
           Toast toast =  Toast.makeText(this, stringBuilder.toString() + System.getProperty("line.separator") ,Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,0, 0);
            toast.show();
        }
    }

    public void Sec_Activity(View view)
    {
        Intent intent = new Intent(activity_main.this, Second_Activity.class);
        startActivity(intent);
    }

    public void AppExit(View view)
    {
        finishAffinity();
    }

    public void onBackPressed(){
        AlertDialog.Builder builder  = new AlertDialog.Builder(activity_main.this);
        builder.setMessage(getResources().getString(R.string.exit_application_check));
        builder.setCancelable(true);
        builder.setPositiveButton(getResources().getString(R.string.button_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        finishAffinity();
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();


    }

}
